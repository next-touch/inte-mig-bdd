SELECT 'User with empty name' AS error
WHERE EXISTS (SELECT 1 FROM public.utilisateurs WHERE trim(nom) = '');

SELECT 'User with empty surname' AS error
WHERE EXISTS (SELECT 1 FROM public.utilisateurs WHERE trim(prenom) = '');

SELECT 'User with empty email' AS error
WHERE EXISTS (SELECT 1 FROM public.utilisateurs WHERE trim(email) = '');

SELECT 'User with empty password' AS error
WHERE EXISTS (SELECT 1 FROM public.utilisateurs WHERE trim(motdepasse) = '');
