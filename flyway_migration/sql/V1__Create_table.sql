CREATE TABLE IF NOT EXISTS public.utilisateurs
(
    Nom character varying(20),
    Prenom character varying(20),
    Email character varying(255),
    MotDePasse character varying(30)
);

ALTER TABLE IF EXISTS public.utilisateurs
    OWNER to postgres;
