# Niveau 1

## Étape 2 : Préparation de la base de données source

### Création des données avec le script `main.py`

Cette étape implique la création de données initiales à l'aide du script Python `main.py`, qui prépare les données pour la migration.
![alt text](photos/image.png)

## Étape 3 : Migration avec DBeaver

### Connexion de MySQL à DBeaver

Établissement d'une connexion entre MySQL et DBeaver pour préparer la migration des données.
![Capture Pre migration](photos/Capture%20Pre%20migration.PNG)

### Création des tables dans PostgreSQL

Dans cette sous-étape, les tables nécessaires sont créées dans PostgreSQL en vue de recevoir les données migrées.
![Capture tables PostgreSQL](photos/Capture%20pre-postgre.PNG)

### Données migrées dans la base PostgreSQL

Après la migration, cette image montre les données maintenant stockées dans la base de données PostgreSQL.
![PostgreSQL après migration](photos/postgres%20post-migration.PNG)

## Étape 4 : Vérification

Vérification que toutes les 10 000 lignes sont correctement migrées et présentes dans PostgreSQL.
![PGAdmin post migration](photos/pgadmin%20verification.PNG)

# Niveau 2

## Étape 1 : Configuration initiale

Configuration des environnements Docker pour MySQL et PostgreSQL, nécessaire pour les étapes de migration à venir.
![PRÉPARATION DOCKER MYSQL](photos/PREPARATION%20DOCKER%20MYSQL.PNG)
![PRÉPARATION DOCKER POSTGRESQL](photos/PREPARATION%20DOKER%20POSTGRE.PNG)

## Étape 2 : Préparation de la base de données MySQL

Préparation initiale de la base de données MySQL pour la migration.
![alt text](photos/image.png)

## Étape 3 : Configuration de Flyway pour la migration

Configuration de l'outil de migration Flyway, incluant la préparation des scripts de migration.
![DOSSIER](photos/DOSSIER.PNG)
![PRÉPARATION DOCKER POSTGRESQL](photos/CONFIG%20FLYWAY.PNG)
![V1 CREATE TABLE](photos/V1%20CREATE%20TABLE.PNG)
![V2](photos/V2.PNG)

## Étape 4 : Lancement de la migration avec Flyway

Exécution de la migration à l'aide de Flyway, avec succès de la migration signalé par les logs de Flyway.
![flyway successful](photos/flyway_successful.PNG)

## Étape 5 : Vérification de la migration

Vérification finale que les données ont été correctement migrées vers PostgreSQL par Flyway.
![verification PostgreSQL Flyway](photos/verification%20postegres%20flyway.PNG)

# Niveau 3

## Étape 1 : Créer des scripts de test

Création de scripts pour tester l'intégrité et la complétude des données après migration.
![test intégrité](photos/test_integrite.PNG)
![test complétude](photos/test_completude.PNG)
![script](photos/script.PNG)

## Étape 2 : Exécuter Flyway

Exécution finale des scripts de migration via Flyway pour confirmer la réussite de toutes les étapes précédentes.
![tests final](photos/tests_final.PNG)
